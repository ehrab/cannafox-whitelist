# Deployed Addresses

Fuji
 - USDC (Tools - ERC20Token): https://testnet.snowtrace.io/address/0x2d9C6bd284c3baAdC21d48bCc57D93b37395623a#code
 - WhitelistMint: https://testnet.snowtrace.io/address/0x3F019b6DA55c77099aD7C6f7DA725bA4B5BBA916#code
 - GenesisClaim: https://testnet.snowtrace.io/address/0x4633c8AaA9456978e0964e9A80d0f6B5c0D2106D#code

Avalanche
 - WhitelistMint: https://snowtrace.io/address/0x8d5790a3019d7C69527c0a1ADDf521981aea169A#code
 - GenesisClaim(deprecated): https://snowtrace.io/address/0x6f3b01657c956Dd4788A9Cc928c1b20d1e61a5CE#writeContract
 - GenesisClaim(v2): https://snowtrace.io/address/0xA09A2f90697f93688e6a7D1f7ae21E8260dB73d0#code




Goerli
 - USDC (Tools - ERC20Token): https://goerli.etherscan.io/address/0x0f09Cf7A7EB2639e2D63856d9e10281FDD568E4d#code
 - WhitelistMint: https://goerli.etherscan.io/address/0xEE43657DfA4003f328CD66537D13E628A3C980b5#code
 - GenesisClaim: https://goerli.etherscan.io/address/0xf84F7701d6b729A1218F35254316797eC766c8e2#code