import { midnightTheme, RainbowKitProvider, ConnectButton } from '@rainbow-me/rainbowkit';
import { chains } from '../../dapp/wagmi/wagmi.ts';

const themeObj = {
	accentColor: '#726FB3',
	accentColorForeground: 'rgba(255, 255, 255, 0.7)',
	borderRadius: 'medium',
};

export const WalletConnect = () => {
	return (
		<RainbowKitProvider chains={chains} theme={midnightTheme(themeObj)} modalSize="compact">
			<ConnectButton />
		</RainbowKitProvider>
	);
};
