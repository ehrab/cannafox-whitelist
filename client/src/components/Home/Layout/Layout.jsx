import { Box, useTheme } from '@mui/material';
import React, { useContext } from 'react';
import { useAccount } from 'wagmi';
import ClaimProvider from '../context/ClaimProvider';
import ErrorBoundary from '../context/ErrorBoundary';
import Header from '../Toolbar/Header';
import CorePanel from './CorePanel/CorePanel';

const Layout = () => {
	const theme = useTheme();
	const { isConnected, isConnecting, isDisconnected } = useAccount();
	return (
		<Box
			id="layout"
			sx={{
				height: '100%',
				backgroundImage: `url(${theme.palette.background.layout})`,
				backgroundSize: 'cover',
			}}
		>
			<Header />
			<Box
				sx={{
					height: '90%',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
				}}
			>
				{isConnecting && <div>Connecting...</div>}
				{isDisconnected && (
					<div>
						Disconnected.
						<br />
						Please connect your wallet.
					</div>
				)}
				{isConnected && (
					<ErrorBoundary>
						<ClaimProvider>
							<CorePanel />
						</ClaimProvider>
					</ErrorBoundary>
				)}
			</Box>
		</Box>
	);
};

export default Layout;
