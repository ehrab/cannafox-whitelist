import React, { useContext, useEffect, useState } from 'react';
import { Card, CardContent, CardHeader, CircularProgress, Divider, Typography, useTheme } from '@mui/material';
import { ClaimContext } from '../../context/context';
import CardData from './CardData';
import Input from './Input';

const CorePanel = () => {
	const theme = useTheme();
	const context = useContext(ClaimContext);
	let { isLoading } = context;
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		console.log('core panel render');
		// setLoading(true);
		// if (!isLoading) {
		// 	return setLoading(false);
		// }
	}, []);

	return isLoading ? (
		<CircularProgress />
	) : (
		<Card
			raised={true}
			sx={{
				[theme.breakpoints.down('xl')]: {
					width: '45%',
				},
				[theme.breakpoints.down('lg')]: {
					width: '55%',
				},
				[theme.breakpoints.down('md')]: {
					width: '65%',
				},
				[theme.breakpoints.down('sm')]: {
					width: '95%',
					top: '20%',
				},
				width: '35%',
				position: 'fixed',
				top: '30%',
			}}
		>
			<CardHeader title={'Purchase'} titleTypographyProps={{ color: 'primary.light' }} />
			<Divider sx={{ borderColor: 'primary.main' }} />
			<CardContent>
				{/* <Input /> */}
				<CardHeader title={'Whitelist Period Is Over!'} titleTypographyProps={{ color: 'warning.main', textAlign: 'center' }} subheader={'Thank you for the support!'} subheaderTypographyProps={{textAlign: 'center'}}  />
				&nbsp;
				<CardData />
			</CardContent>
		</Card>
	);
};

export default CorePanel;
