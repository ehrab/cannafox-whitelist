import React, { useContext, useEffect, useState } from 'react';
import { ClaimContext } from '../../context/context';
import { useContractWrite, useNetwork, usePrepareContractWrite, useWaitForTransaction } from 'wagmi';
import { GENESISCLAIM, USDC } from '../../../../contracts/contracts';
import { ethers } from 'ethers';
import { Button, FormControl, FormHelperText, InputLabel, Stack, TextField, useTheme } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { useDebouncedCallback } from 'use-debounce';
import { useSnackbar } from 'notistack';
import { errorhandler } from '../../../../utilities/helpers';

const { parseUnits } = ethers.utils;
const { BigNumber, constants } = ethers;
const zero = BigNumber.from(0);
const maxUint = constants.MaxUint256;

const Input = () => {
	const theme = useTheme();
	const context = useContext(ClaimContext);
	const { user, terms, usdc, usdcallowance, reservedRemaining, isLoading } = context;
	const userPrice = Number(terms?.price) > 0 ? terms.price : 350;
	const { chain } = useNetwork();
	const explorer = chain.blockExplorers?.default.url;

	const [input, setInput] = useState('');
	const [parsed, setParsed] = useState(zero);
	const [target, setTarget] = useState('approve');
	const [error, setError] = useState(false);
	const [loader, setLoader] = useState(true);
	const [usdcInfo, setUsdcInfo] = useState(usdc);
	const [lastTx, setLastTx] = useState('');
	const { enqueueSnackbar: notify } = useSnackbar();

	const debounce = useDebouncedCallback((value) => {
		// TODO Detect backspace here to clear
		let strvalue = String(value);
		// ? Setting maximum approval now, not sending parsed usdc value
		input && setParsed(target == 'approve' ? parseUnits(strvalue, 6) : BigNumber.from(strvalue));
		console.log('big number ', BigNumber.from(strvalue).toString());
	}, 100);

	// Get Contract by ChainId
	let contract = {
		approve: USDC(chain),
		purchase: GENESISCLAIM(chain),
	};
	let args = {
		approve: contract['purchase'].addressOrName,
	};

	useEffect(() => {
		console.log('render - target: ', target);
		if (!isLoading) {
			if (usdcallowance.gt(zero)) {
				setTarget('purchase');
			} else {
				setTarget('approve');
			}
			setLoader(false);
		}
	}, [context]);

	// ? Dynamically prepare transaction with params for intended function call
	const { config } = usePrepareContractWrite({
		...contract[target],
		functionName: target,
		args: target == 'approve' ? [args['approve'], maxUint] : [parsed],
		onSuccess(data) {
			setError(false);
			console.log(`Prepare Write ${target} success`, data);
		},
		onError(error) {
			console.log(`Prepare Write ${target} failure`);
			setError({ label: errorhandler(error, chain) });
		},
	});

	// Prepared TX object
	const write = useContractWrite(config);

	// ? TX Waiting
	const { isLoading: writeWaiting } = useWaitForTransaction({
		hash: write.data?.hash,
		onSuccess(data) {
			let message = `${target[0].toUpperCase()}${target.substring(1)} Successful! `;
			notify(message);
			setLastTx(`${explorer}/tx/${data?.transactionHash}`);
			setParsed(zero);
		},
		onError(error) {
			notify(errorhandler(error), { variant: 'warning' });
		},
	});

	const handleChange = (event) => {
		let { value } = event.target;
		setInput(value);

		setError(false);
		if (value.length && (isNaN(value) || value < 0)) {
			setError({ ...error, label: 'Please input a whole number!' });
		} else {
			// ? If user balance is greater than the USDC required to purchase the NFT's requested
			if (Number(usdc?.formatted) >= Number(value) * userPrice) {
				debounce(target == 'approve' ? Number(value) * userPrice : value);
			} else if (Number(usdc?.formatted) < Number(value) * userPrice) {
				// ? If user balance is less than the USDC required to purchase the NFT's, check credit, if none, error
				if (terms.credit > 0) {
					debounce(target == 'approve' ? Number(value) * userPrice : value);
				} else {
					target == 'purchase' && setError({ ...error, label: `Balance too low!` });
				}
			}
		}
	};

	const handleMax = () => {
		setError(false);
		reservedRemaining &&
			// Limit of 40/purchase on the contract side
			setInput(reservedRemaining.lte('40') ? reservedRemaining.toString() : '40');
		debounce(reservedRemaining);
		reservedRemaining.isZero() && notify("No allocation for 'max', purchase limit 2 per tx!");
	};

	const handleClick = () => {
		write?.write();
		setInput('');
	};

	const checkAllocation = (value) => {
		if (terms.max - terms.purchased > 0) {
			return true;
		}
		if (terms.max == 0 && Number(value) <= 2) {
			return true;
		} else {
			return setError({
				label: 'Allocation exceeded! (max 2 per tx for public)',
			});
		}
	};

	return (
		<Stack paddingX={1} spacing={1} direction={'column'} justifyContent={'center'}>
			<InputLabel
				children={target === 'purchase' ? 'NFT(s) to Purchase - Max 2 NFTs per tx for public minting.' : 'Click to approve USDC.'}
			/>
			<FormControl>
				<Stack direction={'row'} spacing={1}>
					<TextField
						id="purchase-input"
						fullWidth
						disabled={target === 'approve'}
						onChange={handleChange}
						size="small"
						error={error}
						label={error?.label}
						value={input}
						inputProps={{ style: { borderRight: 'none', borderWidth: 0.5 } }}
						// InputProps={{
						// 	endAdornment: (
						// 		<Button sx={{ px: 0 }} children={"max"} onClick={handleMax} />
						// 	),
						// 	sx: {
						// 		pr: 0,
						// 		borderRight: "none",
						// 		// borderRadius: "4px 0px 0px 4px",
						// 	},
						// }}
					/>
					<LoadingButton
						component={'button'}
						variant="contained"
						loading={loader || write.isLoading || writeWaiting}
						disabled={!write || error.label}
						children={target}
						onClick={handleClick}
						sx={{
							boxShadow: 'none',
							'&:hover': { boxShadow: 'none' },
						}}
					/>
				</Stack>
				{
					<div style={{ height: 20 }}>
						<FormHelperText
							children={
								lastTx && (
									<a
										href={lastTx}
										target="_blank"
										style={{
											color: theme.palette.text.secondary,
											textDecorationColor: theme.palette.text.secondary,
										}}
									>
										Transaction Receipt
									</a>
								)
							}
						/>
					</div>
				}
			</FormControl>
		</Stack>
	);
};

export default Input;
