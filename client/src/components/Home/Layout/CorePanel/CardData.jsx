import { ethers } from 'ethers';
import { Box, CardContent, Stack, Typography, useTheme } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { ClaimContext } from '../../context/context';

const CardData = () => {
	const context = useContext(ClaimContext);
	let { isLoading, terms, cfox, remainingMint, totalReserved, totalPurchased, reservedRemaining, usdc, usdcallowance } = context;
	const { parseUnits } = ethers.utils;
	const [state, setState] = useState({
		price: 0,
		spent: 0,
		purchased: 0,
		max: 0,
	});

	useEffect(() => {
		if (!isLoading) {
			setState({ terms, cfox, remainingMint, totalReserved, totalPurchased, reservedRemaining, usdc, usdcallowance });
		}
	}, [context]);

	return (
		<CardContent>
			<Stack direction={'row'} spacing={2}>
				<Stack direction={'column'} width={'50%'}>
					<Typography pb={1} variant="subtitle1" children={'Your Terms'} color={'secondary.main'} />
					<Stack spacing={1} direction={'column'}>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Price: `} />
							<Typography variant="caption" children={`$${state['terms']?.[0].toString()}`} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Credit: `} />
							<Typography variant="caption" children={`$${state['terms']?.[1].toString()}`} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Purchased: `} />
							<Typography variant="caption" children={`${state['terms']?.[3].toString()}`} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Remaining Reservation: `} />
							<Typography variant="caption" children={`${state['reservedRemaining']?.toString()}`} />
						</Box>
					</Stack>
				</Stack>
				<Stack direction={'column'} width={'50%'}>
					<Typography pb={1} variant="subtitle1" children={'CannaFox NFT Stats'} color={'secondary.main'} />
					<Stack spacing={1} direction={'column'}>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Non Reserved NFTs: `} />
							<Typography variant="caption" children={state['remainingMint']?.toString()} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Reserved NFTs:  `} />
							<Typography variant="caption" children={state['totalReserved']?.toString()} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Total NFTs Purchased:  `} />
							<Typography variant="caption" children={state['totalPurchased']?.toString()} />
						</Box>
						<Box display={'flex'} justifyContent={'space-between'}>
							<Typography variant="caption" color={'text.secondary'} children={`Public mint price:  `} />
							<Typography variant="caption" children={'$350.00'} />
						</Box>
					</Stack>
				</Stack>
			</Stack>
		</CardContent>
	);
};

export default CardData;
