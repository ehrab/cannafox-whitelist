import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { useTheme } from '@mui/material/styles';
import fox from '../../../assets/foxhead.png';
import { WalletConnect } from '../../WalletConnect/WalletConnect';

export default function Header() {
	const theme = useTheme();

	return (
		<Box sx={{ height: '10%', flexGrow: 1 }}>
			<AppBar position="static" sx={{ height: '100%', justifyContent: 'center' }}>
				<Toolbar>
					<IconButton size="large" edge="start" color="inherit" disableRipple>
						<img src={fox} alt="CannaFox" height={25} width={25} />
					</IconButton>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }} color={theme.palette.text.secondary}>
						CannaFox App
					</Typography>
					<WalletConnect />
				</Toolbar>
			</AppBar>
		</Box>
	);
}
