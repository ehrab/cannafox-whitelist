import { createContext } from 'react';

export const ClaimContext = createContext();
ClaimContext.displayName = 'GenesisClaimContract';

export const InputContext = createContext();
InputContext.displayName = 'InputContext';
