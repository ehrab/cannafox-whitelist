import { ethers } from 'ethers';

export const initialState = {
	isError: false,
	isLoading: true,
	termsLoading: false,
	cfoxLoading: false,
	usdcLoading: false,
	terms: null,
	remainingMint: null,
	usdcallowance: ethers.BigNumber.from(0),
	usdc: null,
	reservedRemaining: null,
	totalReserved: null,
	totalPurchased: null,
	cfox: null,
};

export const ClaimReducer = (state, action) => {
	switch (action.type) {
		case 'cfox':
			return {
				...state,
				cfox: action.value,
			};
		case 'terms':
			return {
				...state,
				terms: action.value,
			};
		case 'remainingMint':
			return {
				...state,
				remainingMint: action.value,
			};
		case 'totalReserved':
			return {
				...state,
				totalReserved: action.value,
			};
		case 'totalPurchased':
			return {
				...state,
				totalPurchased: action.value,
			};
		case 'reservedRemaining':
			return {
				...state,
				reservedRemaining: action.value,
			};
		case 'usdc':
			return {
				...state,
				usdc: action.value,
			};
		case 'usdcallowance':
			return {
				...state,
				usdcallowance: action.value,
			};
		case 'loading':
			return {
				...state,
				isLoading: action.value,
			};
		default:
			return state;
	}
};
