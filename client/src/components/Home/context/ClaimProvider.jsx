import { ethers } from 'ethers';
import React, { useEffect, useReducer, useState } from 'react';
import { useAccount, useBalance, useContract, useContractRead, useContractReads, useNetwork } from 'wagmi';
import { USDC, GENESISCLAIM, WHITELISTMINT } from '../../../contracts/contracts';
import { ClaimContext } from './context';
import { ClaimReducer, initialState } from './Reducers';

/**
 * TODO try moving the user and chain hooks into global context, test performance
 * TODO No multicall contracts on avalanche, need to query contracts separately
 */

const ClaimProvider = ({ children }) => {
	const { address: user } = useAccount();
	const { chain } = useNetwork();
	const usdcContract = USDC(chain);
	const genesisClaimContract = GENESISCLAIM(chain);
	const whitelistMint = WHITELISTMINT(chain);

	const [state, dispatch] = useReducer(ClaimReducer, initialState);
	let { cfox, terms, remainingMint, totalReserved, totalPurchased, reservedRemaining, usdc, usdcallowance } = state;

	const [usdcerror, setUsdcerror] = useState(null);
	const [cfoxerror, setcFoxerror] = useState(null);
	const [termserror, setTermserror] = useState(null);

	/**
	 * @notice Check balance of NFT's
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...whitelistMint,
		functionName: 'balanceOf',
		args: [user],
		onSuccess(data) {
			dispatch({ type: 'cfox', value: data });
		},
		onError(error) {
			setcFoxerror(error);
		},
	});

	/**
	 * @notice Check terms of user
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...genesisClaimContract,
		functionName: 'terms',
		args: [user],
		onSuccess(data) {
			dispatch({ type: 'terms', value: data });
		},
		onError(error) {
			setTermserror(error);
		},
	});

	/**
	 * @notice Check Whitelist stats
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...genesisClaimContract,
		functionName: 'remainingMint',
		onSuccess(data) {
			dispatch({ type: 'remainingMint', value: data });
		},
		onError(error) {
			console.log(error);
		},
	});

	/**
	 * @notice Check Whitelist stats
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...genesisClaimContract,
		functionName: 'totalReserved',
		onSuccess(data) {
			dispatch({ type: 'totalReserved', value: data });
		},
		onError(error) {
			console.log(error);
		},
	});

	/**
	 * @notice Check Whitelist stats
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...genesisClaimContract,
		functionName: 'totalPurchased',
		onSuccess(data) {
			dispatch({ type: 'totalPurchased', value: data });
		},
		onError(error) {
			console.log(error);
		},
	});

	/**
	 * @notice Check Whitelist stats
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...genesisClaimContract,
		functionName: 'reservedRemaining',
		args: [user],
		onSuccess(data) {
			console.log(data);
			dispatch({ type: 'reservedRemaining', value: data });
		},
		onError(error) {
			console.log(error);
		},
	});

	/**
	 * @notice Get usdc allowance
	 */
	useContractRead({
		watch: true,
		staleTime: 600_000,
		...usdcContract,
		functionName: 'allowance',
		args: [user, genesisClaimContract.addressOrName],
		onSuccess(data) {
			dispatch({ type: 'usdcallowance', value: data });
		},
		onError(error) {
			console.log(error);
		},
	});

	// USDC balance info
	useBalance({
		// cacheOnBlock: true,
		watch: true,
		staleTime: 600_000,
		addressOrName: user,
		token: usdcContract.addressOrName,
		onSuccess(data) {
			dispatch({ type: 'usdc', value: data });
		},
		onError(error) {
			setUsdcerror(error);
		},
	});

	useEffect(() => {
		if (cfox || terms || remainingMint || totalReserved || reservedRemaining || usdc || usdcallowance || totalPurchased) {
			dispatch({ type: 'loading', value: false });
		}
	}, [cfox, terms, remainingMint, totalReserved, reservedRemaining, usdc, usdcallowance, totalPurchased]);

	return <ClaimContext.Provider value={state}>{children}</ClaimContext.Provider>;
};

export default ClaimProvider;
