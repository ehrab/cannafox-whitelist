export const errorhandler = (error, network) => {
	const { id } = network;
	console.log(`Network id ${id}. ${error}`);

	if (id === 43113 || id === 43114) {
		let errorString = JSON.stringify(error);
		let errorOBJ = JSON.parse(errorString);
		console.log(errorOBJ);
		let reason = errorOBJ?.reason;
		console.log(reason);
		let substring = reason.includes(': ') ? reason.split(': ')[1] : reason;
		console.log(substring);
		return substring;
	}
	return 'error';
};
