import { moonbeamChain, moonbasealphaChain } from "../dapp/chains/moonbeam_info.ts"

// @dev 
// Addresses are also stored in the truffle generated abi files.
// They could be imported and the address could be grabbed programmatically aswell maybe in the contracts.js file or here.
// Chose to do it manually just to avoid some accidental address override in the .json files, then having a bug to chase down.

export const GENESISCLAIM_ADDRESSES = {
	[moonbeamChain.id]: "0xAd4f4deBA41AA59c66d502D9507d0df72246AC43",
	[moonbasealphaChain.id]: "0x9aC3B2FB41aB73f970ADc664c6eC547393332870"
}

export const ANRG_ADDRESSES = {
	[moonbeamChain.id]: "0x432d2e1ad46a9dd0834d248d094dd8e8cf67c82e",
	[moonbasealphaChain.id]: "0x26743fa6d63845EBeC3B4984058675b1Ce57fD5a"
}

export const USDC_ADDRESSES = {
	[moonbeamChain.id]: "0x818ec0a7fe18ff94269904fced6ae3dae6d6dc0b",
	[moonbasealphaChain.id]: "0xb26B601aa753FBA0584655E71d111bfb475fbc8d"
}
