import { Chain } from 'wagmi';

// Create moonBeam chain info object of type Chain
export const moonbeamChain: Chain = {
	id: 1284,
	name: 'Moonbeam',
	network: 'moonbeam',
	nativeCurrency: {
		decimals: 18,
		name: 'Moonbeam',
		symbol: 'GLMR',
	},
	rpcUrls: {
		best: String(process.env.REACT_APP_MOONBEAMRPC),
		wss: String(process.env.REACT_APP_MOONBEAMWSS),
		default: 'https://rpc.api.moonbeam.network',
	},
	blockExplorers: {
		default: { name: 'MoonScan', url: 'https://moonbeam.moonscan.io/' },
	},
	testnet: false,
	multicall: {
		address: '0xcA11bde05977b3631167028862bE2a173976CA11',
		blockCreated: 609002,
	},
};

export const moonbasealphaChain: Chain = {
	id: 1287,
	name: 'Moonbase Alpha',
	network: 'moonbase_alpha',
	nativeCurrency: {
		decimals: 18,
		name: 'Moonbeam DEV',
		symbol: 'DEV',
	},
	rpcUrls: {
		best: String(process.env.REACT_APP_MOONBASERPC),
		wss: String(process.env.REACT_APP_MOONBASEWSS),
		default: 'https://rpc.testnet.moonbeam.network',
	},
	blockExplorers: {
		default: { name: 'MoonScan', url: 'https://moonbase.moonscan.io/' },
	},
	testnet: true,
	multicall: {
		address: '0xcA11bde05977b3631167028862bE2a173976CA11',
		blockCreated: 1850686,
	},
};
