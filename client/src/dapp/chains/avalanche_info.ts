import { Chain } from 'wagmi';

export const avalanche: Chain = {
	id: 43_114,
	name: 'Avalanche',
	network: 'avalanche',
	nativeCurrency: {
		decimals: 18,
		name: 'Avalanche',
		symbol: 'AVAX',
	},
	rpcUrls: {
		default: 'https://api.avax.network/ext/bc/C/rpc',
		fallback: 'https://rpc.ankr.com/avalanche_fuji',
	},
	// multicall: {
	//   address: '0xca11bde05977b3631167028862be2a173976ca11',
	//   blockCreated: 11907934,
	// },
	blockExplorers: {
		default: { name: 'SnowTrace', url: 'https://snowtrace.io' },
	},
	testnet: false,
};

export const fuji: Chain = {
	id: 43_113,
	name: 'Fuji Testnet',
	network: 'avalanche',
	nativeCurrency: {
		decimals: 18,
		name: 'Avalanche',
		symbol: 'AVAX',
	},
	rpcUrls: {
		default: 'https://api.avax-test.network/ext/bc/C/rpc',
		fallback: 'https://rpc.ankr.com/avalanche',
	},
	// multicall: {
	//   address: '0xca11bde05977b3631167028862be2a173976ca11',
	//   blockCreated: 11907934,
	// },
	blockExplorers: {
		default: { name: 'SnowTrace', url: 'https://testnet.snowtrace.io' },
	},
	testnet: false,
};
