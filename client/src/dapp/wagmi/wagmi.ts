import { avalanche, fuji } from '../chains/avalanche_info.ts';
import { connectorsForWallets, wallet } from '@rainbow-me/rainbowkit';
import { configureChains, createClient, defaultChains } from 'wagmi';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';

const staging = process.env.REACT_APP_TESTMODE;

console.log(defaultChains)
let goerliChain = defaultChains[3];

let customDefault =
	staging === 'true'
		? [
				{
					...fuji,
					iconUrl: 'https://chainlist.org/_next/image?url=%2Funknown-logo.png&w=64&q=75',
				},
				goerliChain
		  ]
		: [
				{
					...avalanche,
					iconUrl: 'https://chainlist.org/_next/image?url=https%3A%2F%2Fdefillama.com%2Fchain-icons%2Frsz_avalanche.jpg&w=64&q=75',
				},
		  ];

export const { chains, provider, webSocketProvider } = configureChains(customDefault, [
	jsonRpcProvider({
		rpc: (chain) => ({
			http: chain.rpcUrls.default,
			//  webSocket: chain.rpcUrls.wss
		}),
	}),
	jsonRpcProvider({
		rpc: (chain) => ({
			http: chain.rpcUrls.fallback,
			//  webSocket: chain.rpcUrls.wss
		}),
	}),
]);

const connectors = connectorsForWallets([
	{
		groupName: 'Available',
		wallets: [
			wallet.metaMask({ chains, shimDisconnect: false }),
			wallet.injected({ chains, shimDisconnect: false }),
			wallet.walletConnect({ chains }),
		],
	},
]);

// Create WAGMI client
export const wagmiClient = createClient({
	autoConnect: true,
	connectors: connectors,
	provider,
	webSocketProvider,
});
