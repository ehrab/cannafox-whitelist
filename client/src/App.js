import "./App.css";
// import "./index.css";
import "@rainbow-me/rainbowkit/styles.css";
import { WagmiConfig } from "wagmi";
import { wagmiClient } from "./dapp/wagmi/wagmi.ts";
import backgroundSplash from "./assets/Background.png";
import { SnackbarProvider } from "notistack";
import {
	ThemeProvider,
	createTheme,
	responsiveFontSizes,
	css,
} from "@mui/material/styles";
import Layout from "./components/Home/Layout/Layout";
import fox from "./assets/foxhead.png";

const darkTheme = createTheme({
	palette: {
		mode: "dark",
		background: {
			layout: backgroundSplash,
		},
		text: {
			title: "#FCB21C",
		},
		primary: {
			main: "#726FB3",
		},
	},
	typography: {
		fontFamily: "Sora",
		button: {
			textTransform: "none",
		},
	},
	components: {
		MuiPaper: {
			styleOverrides: {
				root: {
					backgroundColor: "rgba(18, 18, 18, 0.925)",
				},
			},
		},
	},
});

const App = () => {
	return (
		<WagmiConfig client={wagmiClient}>
			<ThemeProvider theme={responsiveFontSizes(darkTheme)}>
				<SnackbarProvider
					iconVariant={{
						default: (
							<img
								src={fox}
								alt="NRG"
								height={25}
								width={25}
								style={{ paddingRight: 10 }}
							/>
						),
					}}
					maxSnack={3}
					anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
				>
					<Layout />
				</SnackbarProvider>
			</ThemeProvider>
		</WagmiConfig>
	);
};

export default App;
