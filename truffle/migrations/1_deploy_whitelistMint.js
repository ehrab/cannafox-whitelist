const WhitelistMint = artifacts.require("WhitelistMint");

module.exports = function (deployer) {
	let deployerAddress = deployer.options["from"];

	deployer.deploy(WhitelistMint, deployerAddress).then(() => {
		console.log("Whitelist contract deployed!")
	});
};
