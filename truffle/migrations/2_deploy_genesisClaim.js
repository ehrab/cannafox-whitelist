const GenesisClaim = artifacts.require("GenesisClaim");
const WhitelistMint = artifacts.require("WhitelistMint");
const ERC20Token = artifacts.require("ERC20Token");

module.exports = async function (deployer, network) {
	const wlAddress = await WhitelistMint.deployed();

	if (network !== 'avalanche') {
		const usdcAddress = await ERC20Token.deployed();
		
		await deployer.deploy(GenesisClaim, wlAddress.address, usdcAddress.address, 100).then(() => {
			console.log("GenesisClaim contract deployed!");
		});
	} else if (network == 'avalanche'){
		const usdcAddress = '0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E';
		
		await deployer.deploy(GenesisClaim, wlAddress.address, usdcAddress, 700).then(() => {
			console.log("GenesisClaim contract deployed!");
		});
	}
};
