const ERC20Token = artifacts.require("ERC20Token");

module.exports = function (deployer, network) {
  let deployerAddress = deployer.options["from"];

  let tokenName = "USD.Coin";
  let tokenSymbol = "USDC";

  network !== 'avalanche' 
    ? deployer
        .deploy(
          ERC20Token,
          tokenName,
          tokenSymbol,
          deployerAddress,
          "1000000000000",
        )
        .then(() => {
          console.log(`${tokenName} contract deployed!`);
        })
    : console.log("Skipping tooling, production network detected!");
};
