const GenesisClaim = artifacts.require("GenesisClaim");
const WhitelistMint = artifacts.require("WhitelistMint");
const ERC20Token = artifacts.require("ERC20Token");

module.exports = async function (deployer, network) {
	const genesisClaim = await GenesisClaim.deployed();
	const whitelistMint = await WhitelistMint.deployed(); 
	
	await deployer.then(async () => {
		if (network == 'fuji') {
			const usdc = await ERC20Token.deployed();
			await usdc.increaseAllowance(genesisClaim.address, "10000000000000").then(() => {
				console.log(`Increase allowance for GenesisClaim contract.`, genesisClaim.address)
			})
		}
		await genesisClaim.approve().then(() => {
			console.log(`Mass approval to save gas.`)
		});
		await whitelistMint.setMinterRole(genesisClaim.address).then(() => {
			console.log(`MINTER_ROLE given to GenesisClaim @ ${genesisClaim.address} by WhitelistMint @ ${whitelistMint.address}.`);
			console.log('Setup finished!');
		});
	})
};
