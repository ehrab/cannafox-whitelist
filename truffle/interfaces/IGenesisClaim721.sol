// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import '@openzeppelin/contracts/token/ERC721/IERC721.sol';
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import '@openzeppelin/contracts/interfaces/IERC2981.sol';

interface IGenesisClaim721 is IERC721, IERC721Enumerable, IERC721Metadata, IERC2981 {
	/**
	 * @notice To call _safeMint insite WhitelistMinter, safeMint not present in erc721
	 * @param to address to receive NFT
	 * @param qty uint256 Quanitity for purchase
	 * @param uri string Uri of the image for mint
	*/
	function safeMint(
		address to,
		uint256 qty,
		string memory uri
	) external;
	
	/**
	 * @notice To call _setDefaultRoyalty
	 * @param _royaltyWallet address
	 * @param _royalty uint96 
	*/
	function setDefaultRoyalty(address _royaltyWallet, uint96 _royalty) external;
}
