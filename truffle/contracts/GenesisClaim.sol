// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import '@openzeppelin/contracts/utils/math/SafeMath.sol';
import '@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol';
import '@openzeppelin/contracts/token/ERC721/IERC721.sol';
import '@openzeppelin/contracts/security/Pausable.sol';
import '@openzeppelin/contracts/access/Ownable.sol';
import '../interfaces/IGenesisClaim721.sol';
import '../interfaces/IERC20plus.sol';
import './tools/Multicall.sol';


/**
 * @title GenesisClaim
 * @author ehrab
 * @notice This contract allows whitelisters to mint NFT'S.
 * @notice Inspired by Zeus of OlympusDAO
 */

interface IClaim {
		// To Interface with old claim contract terms struct
    struct Term {
			uint256 price;
			uint256 credit;
			uint256 spent;
			uint256 purchased;
			uint256 max;
    }

    function terms(address _address) external view returns (Term memory);
}

contract GenesisClaim is Ownable, Pausable, Multicall {
	// ========== DEPENDENCIES ========== //

	using SafeMath for uint256;
	using SafeERC20 for IERC20plus;

	// ========== STRUCTS ========== //
	struct Term {
		uint256 price; // Real terms ie. 50 = $50
		uint256 credit; // Real terms
		uint256 spent; // USDC terms (6 decimals)
		uint256 purchased; // Purchased NFT's
		uint256 reserved; // NFT's Reserved for purchase (record keeping)
	}

	// ========== STATE VARIABLES ========== //
	// Custom interface for GenesisClaim to call erc721
	IGenesisClaim721 public mintContract;
	IERC20plus public usdc;

	string internal presaleNFTUri = 
		"data:application/json;base64,ewoJIm5hbWUiOiAiQ2FubmFGb3guaW8gR2VuZXNpcyBORlQgQ29sbGVjdGlvbiIsCgkiZGVzY3JpcHRpb24iOiAiQW4gTkZUIGZyb20gdGhlIGhpZ2hseSBhY2NsYWltZWQgQ2FubmFG"
		"b3guaW8gY29sbGVjdGlvbi4gVGhlc2UgTkZUcyB3aWxsIGNvcnJlc3BvbmQgMToxIHdpdGggdGhlIGZpbmFsIE5GVCBjb250YWluaW5nIHRoZSBhZGRpdGlvbmFsIHV0aWxpdHkgb2YgVVNEQyBwYXltZW50cyEiLAoJImltY"
		"WdlIjogImlwZnM6Ly9RbVJZTnpTR1lrMTZNUlplU0xwRVRzNHRTVTdScHJhcWRqWEg2Smg5ZVJlYzFmIiwKCSJhdHRyaWJ1dGVzIjogWwoJCXsidHJhaXRfdHlwZSI6ICJHZW5lc2lzIE5GVCIsICJ2YWx1ZSI6ICJGaXJzdC"
		"BFZGl0aW9uIn0KCV0KfQ==";
	address internal collectionWallet = 0x55c388f4f98bf983B056B7E0bd42331481A2C2Ad; // GRiD 1
	address internal royaltyWallet = 0xA5De8CD6Cd0fBC75cd93D919DF579b118F2ca800; // GRiD 2

	IClaim internal immutable previous = IClaim(0x6f3b01657c956Dd4788A9Cc928c1b20d1e61a5CE); // 43114

	/**
	 * @notice Flag for whitelist reservation window
	 */
 	bool public whitelistWindow = true;

	/**
	 * @notice Remaining supply of nft able to be minted.
	 * @dev Start at intended cap, raise as if you're filling a gas tank
	 */
	uint256 public remainingMint;

	/**
	 * @notice Tracks Terms set for address
	 */
	mapping(address => Term) public terms;

	/**
	 * @notice For address change logic
	 */
	mapping(address => address) public walletChange;

	/**
	 * @notice Tracking of reservations and purchases
	 */
	uint256 public totalReserved = 0;
	uint256 public totalPurchased = 0;
	uint256 public reservedPurchased = 0;

	/**
	 * @notice Public Mint price
	 */
	uint256 public publicMint = 350;

	// ========== CONSTRUCTOR ========== //

	constructor(
		address _whitelistMintContract,
		address _usdc,
		uint256 _remainingMint
	)
	{
		mintContract = IGenesisClaim721(_whitelistMintContract);
		usdc = IERC20plus(_usdc);
		remainingMint = _remainingMint;
	}

	// ========== EVENTS ========== //

	event Purchase(uint256 usdcPurchase, uint256 NFTsMinted);

	// ========== MUTABLE FUNCTIONS ========== //

	/**
	 * @notice mass approval saves gas
	 */
	function approve() external whenNotPaused {
		usdc.approve(address(this), 1e33);
	}

	/**
	 * @notice Purchase NFT
	 * @param _nftsToPurchase uint256 How many NFT's requested
	 * @dev the require statement provides additional early validation to the front end when this function is estimated for gas
	 */
	function purchase(uint256 _nftsToPurchase) external whenNotPaused {
		require(remainingMint > 0, "Nothing left to Mint!");
		_purchase(_nftsToPurchase);
	}

	/**
	 * @notice Logic for purchasing, if address does not have WL terms set, proceed with public sale
	 * @param _nftsToPurchase uint256 How many NFT's requested
	 */
	function _purchase(uint256 _nftsToPurchase) internal {
		// Get terms
		Term memory info = terms[msg.sender];
		uint256 usdc_total = 0;
		uint256 mintRemainingModifier = 0;
		uint256 termsReserve = reservedRemaining(msg.sender);
		uint256 decimals = usdc.decimals();

		if (termsReserve == type(uint256).min || termsReserve == 0 || whitelistWindow == false) {
			// ? If public user (never whitelisted) or if whitelist window closed.
			require(_nftsToPurchase <= 2, "Public Mint limit is 2 NFT's per tx!");
			require(_nftsToPurchase <= remainingMint, "Exceeds Remaining for Mint!");

			usdc_total = _nftsToPurchase.mul(publicMint);
			mintRemainingModifier = _nftsToPurchase;

		} else if(termsReserve > 0) {
			// Check so that address cannot purchase more then they have reserved
			require(_nftsToPurchase <= termsReserve, "Claim reserved nfts before buying public!");
			uint256 requested_total = _nftsToPurchase.mul(info.price);

			// Calculate total price of tokens using price and credit from terms
			if (info.credit > 0){
				// Must claim reserved NFTs before minting public
				require((requested_total.sub(info.credit)) == 0, "Claim NFTs credited before making a purchase!");

				usdc_total = 0;
				terms[msg.sender].credit = info.credit.sub(requested_total); 

			} else {
				// ? Has whitelist terms with reserved nfts, but no credit
				usdc_total = requested_total;
			}

			// Update state
			reservedPurchased = SafeMath.add(reservedPurchased, _nftsToPurchase);
		}

		// Process usdc transfer and mint nft
		usdc.safeTransferFrom(msg.sender, collectionWallet, usdc_total.mul(10**decimals));
		mintContract.safeMint(msg.sender, _nftsToPurchase, presaleNFTUri);

		// Update terms
		terms[msg.sender].spent = info.spent.add(usdc_total);
		terms[msg.sender].purchased = info.purchased.add(_nftsToPurchase);

		// Update contract state
		// ? Reservations already factored into remaining, reduce only for public buying
		remainingMint = remainingMint.sub(mintRemainingModifier);
		totalPurchased = SafeMath.add(totalPurchased, _nftsToPurchase);
		emit Purchase(usdc_total, _nftsToPurchase);
	}

	/**
	 * @notice allows address to push terms to new address
	 * @param _newAddress address
	 */
	function pushWalletChange(address _newAddress) external whenNotPaused {
		require(terms[msg.sender].reserved != 0, 'No wallet to change');
		walletChange[msg.sender] = _newAddress;
	}

	/**
	 * @notice allows new address to pull terms
	 * @param _oldAddress address
	 */
	function pullWalletChange(address _oldAddress) external whenNotPaused {
		require(walletChange[_oldAddress] == msg.sender, 'Old wallet did not push');

		// Check to make sure new wallet is not already in storage
		require(terms[msg.sender].reserved == 0, 'Wallet already exists');

		// Zero out old address to disable
		walletChange[_oldAddress] = address(0);
		terms[msg.sender] = terms[_oldAddress];

		// Delete from contract storage
		delete terms[_oldAddress];
	}

	// ========== VIEW FUNCTIONS ========== //

	/**
	 * @notice Reserved remaining for Whitelist Mint
	 * @param _address address to lookup
	 * @return Remaining for purchase
	 */
	function reservedRemaining(address _address) public view returns (uint256) {
		Term memory info = terms[_address];

		if(info.purchased < info.reserved){
			return info.reserved.sub(purchased(_address));
		} else {
			return 0;
		}
	}

	/**
	 * @notice View NFT's purchased by address
	 * @param _address address to lookup
	 * @return Amount of NFT's purchased
	 */
	function purchased(address _address) public view returns (uint256) {
		return terms[_address].purchased;
	}

	/**
	 * @notice View NFT's in circulation
	 * @return Circulating NFT's
	 */
	function circulatingNFT() public view returns (uint256) {
		return mintContract.totalSupply();
	}

	// ========== ADMIN FUNCTIONS ========== //

	/**
	 * @notice End whitelist window and release remaining reservations into public mint pool
	*/
	function endWhitelist() external onlyOwner {
		whitelistWindow = false;
		uint256 overflow = SafeMath.sub(totalReserved, reservedPurchased);
		remainingMint = remainingMint.add(overflow);
		totalReserved = reservedPurchased;
	}

	/**
	 * @notice Wipe remaining mintable supply
	*/
	function wipeMintable() external onlyOwner {
		remainingMint = 0;
	}

	/**
     * @notice bulk migrate users from previous contract
     * @param _addresses address[] memory
		*/
	function migrate(address[] memory _addresses) external onlyOwner {
			for (uint256 i = 0; i < _addresses.length; i++) {
					IClaim.Term memory term = previous.terms(_addresses[i]);
					setTerms(_addresses[i], term.price, term.credit, term.spent, term.purchased, term.max );
			}
	}

	/**
	 *  @notice set terms for new address
	 *  @param _address address
	 *  @param _price uint256 real terms (ie 50 = 50)
	 *  @param _credit uint256 real terms
	 *  @param _spent uint256 usdc spent (6 decimals)
	 *  @param _purchased uint256 NFTs bought
	 *  @param _reserved uint256 NFTs reserved for purchase
	 */
	function setTerms(
		address _address,
		uint256 _price,
		uint256 _credit,
		uint256 _spent,
		uint256 _purchased,
		uint256 _reserved
	) public onlyOwner whenNotPaused {
		require(terms[_address].reserved == 0, 'Address already exists.');
		terms[_address] = Term({ price: _price, credit: _credit, spent: _spent, purchased: _purchased, reserved: _reserved });

		require(_reserved <= remainingMint, "Exceeds maximum supply!");

		uint256 subamount;
		uint256 reservedPurModifier;
		// remaining will be remaining minus what was purchased or reserved. whichever is more in case user buys more on public.
		if (_reserved <= _purchased) { 
			subamount = _purchased;
		} else {
			subamount = _reserved;
		}

		if(_reserved >= _purchased) {
			reservedPurModifier = _purchased;
		} else if (_purchased > _reserved) {
			// Means they bought extra on public
			reservedPurModifier = _reserved;
		} else {
			reservedPurModifier = 0;
		}
		
		reservedPurchased = reservedPurchased.add(reservedPurModifier);
		remainingMint = remainingMint.sub(subamount);
		totalReserved = totalReserved.add(_reserved);
		totalPurchased = totalPurchased.add(_purchased);
	}

	/**
	 * @notice Set receiving address
	 * @param _royalty address
	*/
	function setRoyalty(address _royalty) external onlyOwner {
		royaltyWallet = _royalty;
	}

	/**
	 * @notice Set collection address
	 * @param _collection address
	*/
	function setCollection(address _collection) external onlyOwner {
		collectionWallet = _collection;
	}

	/**
	 * @notice For admin to set new NFT Uri
	 * @param _uri string 
	*/
	function setUri(string memory _uri) external onlyOwner {
		presaleNFTUri = _uri;
	}

	/**
	 * @notice For admin to set price for future mints prior to launch
	 * @param _price uint256 Real terms 10 = $10
	*/
	function setPublicMintPrice(uint256 _price) external onlyOwner {
		require(_price > publicMint, "Cannot lower price, only increase!");
		publicMint = _price;
	}

	function pause() external onlyOwner {
		_pause();
	}

	function unpause() external onlyOwner {
		_unpause();
	}

	function setMintContract(address _minter) external onlyOwner {
		mintContract = IGenesisClaim721(_minter);
	}

	function addMintSupply(uint256 _amount) external onlyOwner {
		remainingMint = SafeMath.add(remainingMint, _amount);
	}
}
