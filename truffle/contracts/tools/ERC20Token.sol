// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


// mock class using ERC20
contract ERC20Token is ERC20, Ownable {
    constructor(
        string memory name,
        string memory symbol,
        address initialAccount,
        uint256 initialBalance
    ) payable ERC20(name, symbol) {
        _mint(initialAccount, initialBalance);
    }

		/// @notice Ungated mint for easy access to USDC
    function mint(address account, uint256 amount) public {
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) public {
        _burn(account, amount);
    }

		/// @notice Admin control over balances
    function transferInternal(
        address from,
        address to,
        uint256 value
    ) public onlyOwner {
        _transfer(from, to, value);
    }

    function approveInternal(
        address owner,
        address spender,
        uint256 value
    ) public onlyOwner {
        _approve(owner, spender, value);
    }

		// ========== OVERRIDES ========== //

		function decimals() public pure override returns (uint8) {
			return 6;
		}
}
