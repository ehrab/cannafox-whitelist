// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721Royalty.sol';
import '@openzeppelin/contracts/security/Pausable.sol';
import '@openzeppelin/contracts/access/AccessControl.sol';
import '@openzeppelin/contracts/utils/Counters.sol';
import '@openzeppelin/contracts/utils/math/SafeMath.sol';

/**
 * @title CannaFox Whitelist Mint contract
 * @author ehrab
 * @notice Contract is called by GenesisClaim.sol using AccessControl
 */
contract WhitelistMint is ERC721, ERC721URIStorage, ERC721Enumerable, ERC721Burnable, ERC721Royalty, Pausable, AccessControl {
	// ========== DEPENDENCIES ========== //
	using SafeMath for uint256;

	// ============ STATE VARIABLES ============ //
	using Counters for Counters.Counter;

	bytes32 public constant PAUSER_ROLE = keccak256('PAUSER_ROLE');
	bytes32 public constant MINTER_ROLE = keccak256('MINTER_ROLE');
	bytes32 public constant BURNER_ROLE = keccak256('BURNER_ROLE');
	Counters.Counter private _tokenIdCounter;

	// ============ CONSTRUCTOR ============ //
	/**
	 * @notice Set the authority for minting at deployment
	 * @dev To avoid the circular issue of contracts needing to know each other, set the Minter Role AFTER deployment
	 * @param _royaltyWallet Set wallet to receive royalties
	 */
	constructor(address _royaltyWallet) ERC721('CannaFox.io', 'aCFOX') {
		_grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
		_grantRole(PAUSER_ROLE, msg.sender);
		_grantRole(MINTER_ROLE, msg.sender);
		_grantRole(BURNER_ROLE, msg.sender);
		_setDefaultRoyalty(_royaltyWallet, 500);
	}

	// ============ MUTABLE FUNCTIONS ============ //
	/**
	 * @notice Mint NFT after purchase in GenesisClaim
	 * @param to address to receive NFT
	 * @param nfts uint256 Nft's to purchase
	 * @param uri string Nft image data
	 */
	function safeMint(
		address to,
		uint256 nfts,
		string memory uri
	) public onlyRole(MINTER_ROLE) {
		for (uint256 i = nfts; i > 0; i--) {
			uint256 tokenId = _tokenIdCounter.current();
			_tokenIdCounter.increment();
			_safeMint(to, tokenId);
			_setTokenURI(tokenId, uri);
		}
	}

	// ============ HOOKS ============ //

	function _beforeTokenTransfer(
		address from,
		address to,
		uint256 tokenId
	) internal override(ERC721, ERC721Enumerable) whenNotPaused {
		super._beforeTokenTransfer(from, to, tokenId);
	}

	// ============ ADMIN ============ //

	/**
	 * @notice The below 3 functions are to set Roles easier for team
	 * @param _address address To grant authority
	 * @return Address that had Role applied
	 */
	function setMinterRole(address _address) external onlyRole(DEFAULT_ADMIN_ROLE) returns (address) {
		_grantRole(MINTER_ROLE, _address);
		return _address;
	}

	function setPauserRole(address _address) external onlyRole(DEFAULT_ADMIN_ROLE) returns (address) {
		_grantRole(PAUSER_ROLE, _address);
		return _address;
	}

	function setBurnerRole(address _address) external onlyRole(DEFAULT_ADMIN_ROLE) returns (address) {
		_grantRole(BURNER_ROLE, _address);
		return _address;
	}

	/**
	 * @notice Burn alpha NFT's by ID
	 * @param tokenId uint256
	 */
	function burn(uint256 tokenId) public override(ERC721Burnable) onlyRole(BURNER_ROLE) {
		_burn(tokenId);
	}

	function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage, ERC721Royalty) {
		super._burn(tokenId);
	}

	/**
	 * @notice Set the royalty amount on the collection
	 * @dev Use this admin function if the receiving address needs to change
	 * @param _royaltyWallet address
	 */
	function setDefaultRoyalty(address _royaltyWallet) external onlyRole(DEFAULT_ADMIN_ROLE) {
		_setDefaultRoyalty(_royaltyWallet, 500);
	}

	function pause() public onlyRole(PAUSER_ROLE) {
		_pause();
	}

	function unpause() public onlyRole(PAUSER_ROLE) {
		_unpause();
	}

	// ============ VIEW FUNCTIONS ============ //
	function tokenURI(uint256 tokenId) public view override(ERC721, ERC721URIStorage) returns (string memory) {
		return super.tokenURI(tokenId);
	}

	function supportsInterface(bytes4 interfaceId) public view override(ERC721, AccessControl, ERC721Enumerable, ERC721Royalty) returns (bool) {
		return super.supportsInterface(interfaceId);
	}
}
