require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');

// NOTE: Do not store your private key in plaintext filesa
//       this is only for demostration purposes only
const deployerKeyGnache = process.env.GNACHE_DEV;

const deployerKeyAvalanche = process.env.LINKPOINT_DEPLOYER;
const deployerKeyFuji = process.env.FUJI_DEPLOYER;

const avalancheRPC = process.env.AVALANCHE_POKT;
const fujiRPC = process.env.FUJI_RPC;

const rinkebyRPC = process.env.RINKEBY_RPC;
const goerliRPC = process.env.GOERLI_RPC;
const deployerEthTestnet = process.env.ETHTESTNET_DEPLOYER;

const snowtraceKey = process.env.SNOWTRACE_KEY;
const etherscanKey = process.env.ETHERSCAN_KEY;

module.exports = {
	 dashboard: {
		port: 24012,
	 }, 
   networks: {
      // Local Development Network
      dev: {
         provider: () => {
            if (!privateKeyDev.trim()) {
               throw new Error(
                  'Please enter a private key with funds, you can use the default one'
               );
            }
            return new HDWalletProvider(
               privateKeyDev,
               'http://localhost:9933/'
            );
         },
         network_id: 1281,
      },
      // Avalanche Mainnet
      avalanche: {
         provider: () => {
            if (!deployerKeyAvalanche.trim()) {
               throw new Error(
                  'Please enter a private key with funds to send transactions to TestNet'
               );
            }
            if (deployerKeyAvalanche == deployerKeyFuji) {
               throw new Error(
                  'Please change the private key used for Fuji to different one for Avalanche with funds'
               );
            }
            return new HDWalletProvider(
               deployerKeyAvalanche,
               avalancheRPC
            );
         },
         network_id: 43114,
      },
      // Fuji Testnet
      fuji: {
         provider: () => {
            if (!deployerKeyFuji.trim()) {
               throw new Error(
                  'Please enter a private key with funds to send transactions to TestNet'
               );
            }
            if (deployerKeyAvalanche == deployerKeyFuji) {
               throw new Error(
                  'Please change the private key used for Avalanche to different one for Fuji with funds'
               );
            }
            return new HDWalletProvider(
               deployerKeyFuji,
               fujiRPC
            );
         },
         network_id: 43113,
      },
      // Moonbase Alpha TestNet
      moonbase: {
         provider: () => {
            if (!privateKeyMoonbase.trim()) {
               throw new Error(
                  'Please enter a private key with funds to send transactions to TestNet'
               );
            }
            if (privateKeyDev == privateKeyMoonbase) {
               throw new Error(
                  'Please change the private key used for Moonbase to your own with funds'
               );
            }
            return new HDWalletProvider(
               privateKeyMoonbase,
               moonbaseRPC
            );
         },
         network_id: 1287,
      },
			moonbeam: {
				provider: () => {
					if (!privateKeyMoonbase.trim()) {
						throw new Error(
							 'Please enter a private key with funds on Moonbeam to send transactions'
						);
					}
					if (privateKeyDev == privateKeyMoonbeam) {
							throw new Error(
								'Please change the private key used for Moonbeam to your own with funds'
							);
					}
					return new HDWalletProvider(
						privateKeyMoonbeam,
						moonbeamONFINALITY
					)
				},
				network_id: 1284,
			},
			rinkeby: {
				provider: () => {
					if (!deployerEthTestnet.trim()) {
						throw new Error(
							 'Please enter a private key with funds on rinkeby to send transactions'
						);
					}
					return new HDWalletProvider(
						deployerEthTestnet,
						rinkebyRPC
					)
				},
				network_id: 4,
			},
			goerli: {
				provider: () => {
					if (!deployerEthTestnet.trim()) {
						throw new Error(
							 'Please enter a private key with funds on Goerli to send transactions.'
						);
					}
					return new HDWalletProvider(
						deployerEthTestnet,
						goerliRPC
					)
				},
				network_id: 5,
			},
			gnache: {
				provider: () => {
					 return new HDWalletProvider(
							privateKeyGnache,
							'http://127.0.0.1:7545'
					 );
				},
				network_id: 5777,
		 },
   },
   compilers: {
      solc: {
				version: "pragma",
				settings: {
					optimizer: {
							enabled: true,
							runs: 20000
					}
				}
      },
   },
   // Truffle Plugin & Truffle Plugin for Verifying Smart Contracts
   plugins: ['truffle-plugin-verify'],
	 api_keys: {
		 snowtrace: snowtraceKey, etherscan: etherscanKey
	 }
};
